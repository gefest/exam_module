from setuptools import setup


setup(
    name='exam_module',  # package name
    version='0.1',  # version
    description='MY Exam MODULE',  # short description
    install_requires=[],  # list of packages this package depends
    packages=['exam_module'],
    package_dir={"exam_module": "src"},
)
